#!/bin/bash -e

BaseWorkingDirectory="$1"
shift

shopt -s extglob
BaseWorkingDirectory2=${BaseWorkingDirectory/_E+([0-9])/_E0}
CurrentWorkingDirectory2=$(pwd)
CurrentWorkingDirectory2=${CurrentWorkingDirectory2/_E+([0-9])/_E0}

echo "Splitting debug info from $1"
docker run --rm -i -v "$BaseWorkingDirectory2:$BaseWorkingDirectory" -u $(id -u):$(id -g) -w "$CurrentWorkingDirectory2" proget.lubar.me/dfhack-docker/build-env dsymutil "$1"
mkdir -p "$2"
rm -rf "$2/$1.dSYM"
mv "$1.dSYM" "$2/"

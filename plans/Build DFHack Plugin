##AH:EvaluateStatementDescriptions


set $GitHubBuildContext = "";

if $IsVariableDefined(OverrideStructuresCommit)
{
    set $GitHubBuildContext = auto-structures;
}
else
{
    if $PluginTag != ""
    {
        Set-Execution-Priority 50;

        set $GitHubBuildContext = release;
    }
    else
    {
        if $DFHackTag != ""
        {
            Set-Execution-Priority 10;

            set $GitHubBuildContext = pre-$DFHackTag;
        }
        else
        {
            Set-Execution-Priority 30;

            set $GitHubBuildContext = develop;
        }
    }
}

if $GetVariableValue(TriggeredByDFHack)
{
    Set-Execution-Priority 5;
}

try
{
    GitHub-Set-Status
    (
        Credentials: GitHub-DFHack,
        Organization: $PluginOwner,
        Repository: $PluginRepo,
        AdditionalContext: $GitHubBuildContext,
        CommitHash: $PluginCommit,
        Description: "#$ExecutionId downloading source code...",
        Status: pending
    );

    # Get Source
    {
        # Git
        with lock = !DFHackGit
        {
            foreach server in @ServersInRole(DFHack-Linux-Builders)
            {
                # Linux ($ServerName)
                with async = Git
                {
                    call dfhack-raft::DFHack-Plugin-Get-Sources();
                }
            }

            await Git;
        }

        GitHub-Set-Status
        (
            Credentials: GitHub-DFHack,
            Organization: $PluginOwner,
            Repository: $PluginRepo,
            AdditionalContext: $GitHubBuildContext,
            CommitHash: $PluginCommit,
            Description: "#$ExecutionId downloading dependencies...",
            Status: pending
        );

        with lock = !DFHackDependencies
        {
            # Linux
            foreach server in @ServersInRole(DFHack-Linux-Builders)
            {
                # Linux Dependencies ($ServerName)
                {
                    with async = Dependencies
                    {
                        Create-File $WorkingDirectory/src/plugins/CMakeLists.custom.txt
                        (
                            Text: "add_subdirectory($ApplicationName)",
                            Overwrite: true
                        );
                    }

                    with async = Dependencies
                    {
                        call dfhack-raft::DFHack-Cache-Dependencies
                        (
                            DependencyPath: $WorkingDirectory/src/CMake/downloads
                        );
                    }

                    with async = Dependencies
                    {
                        InedoCore::Exec
                        (
                            FileName: /usr/bin/docker,
                            Arguments: pull $DFHackBuildEnv:latest,
                            ErrorOutputLogLevel: Debug
                        );
                    }

                    with async = Dependencies
                    {
                        InedoCore::Exec
                        (
                            FileName: /usr/bin/docker,
                            Arguments: pull $DFHackBuildEnv:gcc-4.8,
                            ErrorOutputLogLevel: Debug
                        );
                    }

                    with async = Dependencies
                    {
                        InedoCore::Exec
                        (
                            FileName: /usr/bin/docker,
                            Arguments: pull $DFHackBuildEnv:msvc,
                            ErrorOutputLogLevel: Debug
                        );
                    }

                    with async = Dependencies
                    {
                        InedoCore::Exec
                        (
                            FileName: /usr/bin/mkdir,
                            Arguments: -p `$HOME/.ccache out32 out32-4.8 out32-osx out32-osx-4.8 out32-win out64 out64-4.8 out64-osx out64-osx-4.8 out64-win build32 build32-4.8 build32-osx build32-osx-4.8 build32-win build64 build64-4.8 build64-osx build64-osx-4.8 build64-win,
                            WorkingDirectory: $WorkingDirectory
                        );
                    }
                }
            }

            await Dependencies;
        }
    }

    GitHub-Set-Status
    (
        Credentials: GitHub-DFHack,
        Organization: $PluginOwner,
        Repository: $PluginRepo,
        AdditionalContext: $GitHubBuildContext,
        CommitHash: $PluginCommit,
        Description: "#$ExecutionId waiting to build...",
        Status: pending
    );

    if $PluginTagged
    {
        call dfhack-raft::DFHack-Plugin-Build
        (
            BuildType: Release,
            GitHubBuildContext: $GitHubBuildContext
        );
    }
    else
    {
        call dfhack-raft::DFHack-Plugin-Build
        (
            BuildType: RelWithDebInfo,
            GitHubBuildContext: $GitHubBuildContext
        );
    }
}
catch
{
    error;
}

GitHub-Set-Status
(
    Credentials: GitHub-DFHack,
    Organization: $PluginOwner,
    Repository: $PluginRepo,
    AdditionalContext: $GitHubBuildContext,
    CommitHash: $PluginCommit,
    Status: auto
);
